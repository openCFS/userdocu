Common Visualization Tasks
=========================

## Loading a CFS result file

1. In the *menu bar* go to *File → Open...* and then select the result file from CFS, e.g. `results_hdf5/run.cfs`
2. Mark the result in the Pipeline Browser and tick/untick the Regions you want to load in the *Properties tab*
3. click *Apply* in the Properties tab

## Visualizing Field Results

After opening a result file

1. Mark the result in the *Pipeline Browser*
2. Select which result to display in the drop-down of the *Active Variable Controls* toolbar
  >  The icon shows if results are defined on the nodes (points) or in the center of the elements (cell symbol).
3. Use the *Representation Toolbar* to choose the desired appearance
  >  Most useful are **Surface With Edges** to show element boundaries, or just **Surface**.

## Plotting Vector Fields

After opening a result file and marking it in the *Pipeline Browser*

1. Use the *Glyph* filter in the *Common* toolbar (or e.g. *Filters → Search...*).
2. Select the vector valued data.
3. Click *Apply*.
4. Choose the *Coloring* in the *Properties* tab of the filter (or in the *Active Variable Controls* toolbar).

> The *Scaling* options can be used to adapt the size of the vectors. For example, by choosing the vector field in *Scale Array* and *Scale by Magnitude* for the *Vector Scale Mode*, the vectors are displayed according to their magnitude, multiplied by the *Scale Factor*.

> The *Masking* option determines the number of vectors that are displayed.

The steps are also visualized in the gif below:

![](Paraview_vectorfield.gif)

## Show Deformed Geometry

After opening a result file and marking it in the *Pipeline Browser*

1. Use the *Warp By Vector* filter in the *Common* toolbar (or e.g. *Filters → Search...*).
2. Select the desired data for *Vectors*.
3. Specify the *Scale Factor*.
4. Click *Apply*.
5. Optionally, choose the *Coloring* in the *Properties* tab of the filter (or in the *Active Variable Controls* toolbar) and show the undeformed structure, too.
  > Click the *eye* symbol in the *Pipeline Browser* to toggle the display of certain pipeline elements.

The steps are also visualized in the gif below:

![](Paraview_warp.gif)

## Show Multi-Step Results

1. After loading the result file, the desired *Analysis Step* can be selected.
  > *Analysis Step* corresponds to the *index* of the *sequenceStep* in the input-xml-file.

2. Click *Apply* to update your choice.

![](multistep.png)

## Save a Screenshot

In order to save a screenshot of your *View*, the following steps are necessary.

1. *File → Save Screenshot*
2. Choose file location.
3. Select desired *Size*, *Scaling*, *Coloring* and *Compression Level*.


## Animate Harmonic Results in Paraview

In this tutorial is a simple example used to show how to animate harmonic results in paraview. As Simulation we use the [cantilever application](../../Applications/Singlefield/Mechanics/Cantilever/README.md) and visualize the deformation via *Warp by vector* and animate the harmonic displacement.

![](cantilever.gif)

1. Open CFS-file.
2. Choose needed *Region*.
3. Check **Animate Harmonics/Modal Results**.
4. Hit *Apply*.
5. Choose right *Analysis Step*.
6. Choose right *Frequency Step*.
7. Hit *Apply*.

Now we want to visualize the displacement field, therefore:

8. Select *Warp by Vector* (to see some cool deformation).
9. Hit *Apply*.
10. Select under Vectors **mechDisplacement-mode** (Its important to always select the **mode-result**, if choosen amplitude the direction is lost).
11. Select a good *Scale Factor*.
12. Hit *Apply*,
13. Press play and enjoy a nice animation.

All the steps above are visualized as a gif as well:

![](ani.gif)


>To get the animation smoother, you can change the frames per second and many more under _`View => Animation View`_

>You can also animate scalarfields (e.g. temperature field), vectorfields (e.g. magnetic flux density) and many more.


## Save an Animation

After setting up an animation as shown above, the animation can be saved as follows:

1. Select *File → Save Animation*.
2. Choose file location and file name.
3. Select desired *Size*, *Scaling*, *Coloring* and *File -* and *Animation Options*.
  > If you select an image file type ParaView will save individual frames called `name.NNNN.ext`
