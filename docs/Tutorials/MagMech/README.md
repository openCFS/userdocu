# Magnetic-Mechanic Coupling

In this tutorial we consider an axi-symmetric model of a coil and a plate as depicted in the figure.
The inner and outer radii of the coil, $r_1$ = 0.0025 m and $r_2$ = 0.0075 m.
The length of the coil, $l_1$ = 0.008 m.
The plate thickness and air gap is $l_2$ = 5 $\cdot 10^{-4}$ m respectively $l_3$ = 7.5 $\cdot 10^{-4}$ m.
The point $C$ is located in the center of the coil, the point P in the middle of the plate at $z$ = 0.005 m.
The plate is either made from iron or aluminum, with electric conductivity and linear magnetic permeability given in the `mat.xml`.
The coil has 40 turns and is fed by a controlled current of $i$ = 10 kA. 

![sketch](sketch.png){: style="width:200px"}

---------------------------
## Tasks
1. Create a mesh using Cubit and [geometry.jou](geometry.jou) (Hint: you can use the command `cubit  -batch -nojournal -nographics geometry.jou` to create the mesh with the terminal.)
2. Simulate the transient magnet with openCFS and use the following input files: [coil_transient.xml](coil_transient.xml) and [amt.xml](mat.xml)
## Tutorial Suggestions
- Look at the magnetic field lines ( with paraview: stream trace)
- Have a look at the iterative coupling section in [coil_transient.xml](coil_transient.xml)
- Visualize the time signal of the z-displacements of point $P$ for both plate-materials in one figure
- Plot the displacement of the plate for both plate-materials.
- Describe the time signals: Are there transient effects? What is the difference between the amplitudes with aluminum and iron plate and why is there a difference?

## Further examples in the testsuite
If you need more examples, please have a look at [our examples from the testsuite](https://gitlab.com/openCFS/Testsuite/-/tree/master/TESTSUIT/Coupledfield/MagMech)