#!/bin/sh

# mesh (creates domain.cdb)
cubit -batch -nographics -nojournal pipe.jou

# run cfs
cfs -p mech-acou-eigenvalues.xml test
