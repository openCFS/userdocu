#!/bin/sh

# mesh (creates domain.cdb)
trelis -batch -nographics -nojournal domain.jou

# run cfs
cfs -p transient.xml run-transient

# combine the sensor array result files `history/transient-acouPressure-monitor-<NN>`
# which creates file history/transient-acouPressure-monitor.hist
ipython combineSensorArray.py history/transient-acouPressure-monitor
