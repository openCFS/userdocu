Using Gmsh for openCFS
======================

You can use [Gmsh](https://gmsh.info/) as a mesh gnerator for openCFS.
Gmsh can be nicely scripted using it's ASCII `*.geo` input files.
To get started check the [Tutorial section of the Gmsh documentation](https://gmsh.info/doc/texinfo/gmsh.html#Tutorial).

Basic usage
-----------

1. create geometry in the GUI or by editing the `*.geo` file
2. create _Physical Volume_, _Physical Surface_, ... to define region names
3. export the mesh file in the correct format `gmsh -3 -format msh2 my-mesh.geo` which should create the ASCII mesh file `my-mesh.msh`

Interface to openCFS
--------------------

A few things are important when using cubit as a mesh gnerator:

* Region names are defined by _physical groups_.
* You need to use the legacy mesh format when exporting the mesh.
