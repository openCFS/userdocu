# [pyCFS](https://gitlab.com/openCFS/pycfs)

pyCFS is an automation and data processing library for the [openCFS](https://opencfs.org/) software. It enables the user to build an abstraction
layer around the openCFS simulation which means that the user can execute simulations directly from a python script or notebook without worrying 
about the individual simulation files.

Furthermore, the data processing submodule implements the most frequently needed data processing building
blocks and pipelines which are employed in data pre and postprocessing steps.

## [Installation](https://opencfs.gitlab.io/pycfs/installation.html)

Install via [pip](https://pypi.org/project/pyCFS/)

```sh
pip install pycfs
```

## [Documentation](https://opencfs.gitlab.io/pycfs/)

Documentation of [pyCFS](https://pypi.org/project/pyCFS/) can be found [here](https://opencfs.gitlab.io/pycfs/).

## [pyCFS-data](https://opencfs.gitlab.io/pycfs/pycfs_data.html)

Data processing framework for [openCFS](https://opencfs.org/).[@wurzinger2024PyCFSdataDataProcessing] This project contains Python libraries to easily create and
manipulate data stored in openCFS type HDF5 file format (`*.cfs`). A small tutorial for using some of the main features of the pyCFS-data module can be found [here](https://opencfs.gitlab.io/pycfs/pycfs_data.html#tutorial).

<details>
  <summary>Example code</summary>
```python
from pyCFS.data.io import CFSReader, CFSWriter

with CFSReader('file.cfs') as f:
    mesh = f.MeshData
    results = f.MultiStepData
with CFSWriter('file.cfs') as f:
    f.create_file(mesh_data=mesh, result_data=results)
```
</details>

* [CFS I/O](https://opencfs.gitlab.io/pycfs/pycfs_data.html#cfs-io)
    - [Reader class](https://opencfs.gitlab.io/pycfs/generated/pyCFS.data.io.CFSReader.html) containing top and low-level methods for reading `*.cfs`
    - [Writer class](https://opencfs.gitlab.io/pycfs/generated/pyCFS.data.io.CFSWriter.html) containing top and low-level methods for writing `*.cfs`
* [Operators](https://opencfs.gitlab.io/pycfs/pycfs_data.html#operators)
    - [Transformation operators](https://opencfs.gitlab.io/pycfs/generated/pyCFS.data.operators.transformation.html)
    - [Interpolators](https://opencfs.gitlab.io/pycfs/generated/pyCFS.data.operators.interpolators.html)
* [Extra functionality](https://opencfs.gitlab.io/pycfs/pycfs_data.html#extra-functionality): manipulate data from various formats

# References
\bibliography