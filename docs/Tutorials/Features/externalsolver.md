# External Solver

An external program can be used to solve the linear system $\mathbf{Ax}=\mathbf{b}$. The basic idea of the external solver is, that CFS exports the system matrix $\mathbf{A}$ and the right-hand-side $\mathbf{b}$ and the external solver returns the solution $\mathbf{x}$ to CFS. The external solver can be used for the analysis types: static, harmonic and transient. The example [Model Order Reduction with External Solver](../../Applications/Singlefield/Mechanics/ModelOrderReduction/bendingBeam.png) gives an idea how the external solver can be used.

## Requirements for the external solver
As an external solver any program installed on your computer fullfilling the following conditions can be used:

* Capable of reading and writing matrices into files in the [Matrix-Market Format](../Features/externaleigensolver.md#matrix-market-format').
* Capable of solving the linear system $\mathbf{Ax}=\mathbf{b}$
* The program needs to be callable via the command line. Note, the command, calling the external solver, will be executed in the working directory CFS was started from.

In the example of the [Model Order Reduction with External Solver](../../Applications/Singlefield/Mechanics/ModelOrderReduction/bendingBeam.png) a python script is used, but be aware that this is not the only option and any other executable fullfilling the conditions can be used.

## XML-Scheme

The behavior and transfered information of the external solver is set in the xml-Scheme. In the `<linearSystem>` sections `<system>` and `<solverlist>` the tag `<externalSolver>` can be selected. In order to run, at least the following tag needs to be specfied:

* `<cmd>` defines how the external solver is called via the command line to solve the linear system. Again note, the command, calling the external solver, will be executed in the working directory CFS was started from.

All the other settings are not mandatory:

* `<logging>` if 'yes', the command calling the external solver and its output, will be displayed in the terminal during the simulation.

* `<arguments>` defines the additional arguments, which will be added to the call in the command line. The order of the arguments in the command will be the same as in the xml-Scheme.

    * `<matrixFileName>` specfies the filename of the exported system matrix in [Matrix-Market Format](../Features/externaleigensolver.md#matrix-market-format'), excluding the file extension `.mtx`. Note, that it is not necessary to specify a filename. If no filename is specfied, but the tag `<matrixFileName>` exists, the defaul filename will be used: 'JOB_mat_MultiSequenceStepNumber_StepNumber'. This results for example in 'myjob_mat_1_2.mtx' for the second step of the first sequence-step of job myjob.
  
    * `<rhsFileName>` specfies the filename of the exported right-hand-side vector in [Matrix-Market Format](../Features/externaleigensolver.md#matrix-market-format'), excluding the file extension `.mtx`. Note, that it is not necessary to specify a filename. If no filename is specfied, but the tag `<rhsFileName>` exists, the defaul filename will be used: 'JOB_rhs_MultiSequenceStepNumber_StepNumber'. This results for example in 'myjob_rhs_1_2.mtx' for the second step of the first sequence-step of job myjob.

    * `<solutionFileName>` specfies the filename of the imported solution vector in [Matrix-Market Format](../Features/externaleigensolver.md#matrix-market-format'), excluding the file extension `.mtx`. Note, that it is not necessary to specify a filename. If no filename is specfied, but the tag `<solutionFileName>` exists, the defaul filename will be used: 'JOB_sol_MultiSequenceStepNumber_StepNumber'. This results for example in 'myjob_sol_1_2.mtx' for the second step of the first sequence-step of job myjob.
  
    * `<arg>` defines a string which will appear in the command. This element can occur multiple times. 
  
    * `<step>` writes the current step of the analysis in the command. Considering an harmonic or transient solution, a linear system has to be solved for every frequency or respectively time step. The step of solving the linear system of the first frequency would correspond with `step = 1`.
  
    * `<timeFreq>` writes the time or frequency in the current step into the command with 6 digits. It has the attribute `formatString`, where the format of the output can be set with format specifier, similar as in the C/C++-function [`printf`](https://en.cppreference.com/w/cpp/io/c/fprintf). Note, that the time/frequency has the datatype of a double.
  
* `<deleteFiles>` if 'yes', the files of the system matrix, the right-hand-side vector and the solution vector, specified above, will be deleted after the simulation.

The following xml-input for the external solver, using a python script, 
```
<externalSolver>
    <logging>yes</logging>
    <cmd>python3 solve.py</cmd>
    <arguments>
        <matrixFileName>matrix</matrixFileName>
        <rhsFileName>rhs</rhsFileName>
        <solutionFileName>solution</solutionFileName>
        <arg>-f</arg>
        <timeFreq formatString="%1.3f"/>
        <step />
    </arguments>
    <deleteFiles>no</deleteFiles>
</externalSolver>
```
leads to the following command, if it is for example the second step of an harmonic analysis at the frequency of 112 Hz.
```
python3 solve.py matrix.mtx rhs.mtx solution.mtx -f 112.000 2
```


