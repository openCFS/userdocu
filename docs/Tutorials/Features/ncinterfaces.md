# Non-Conforming Interfaces

Non-conforming interfaces allow the usage of non-matching, interacting grids connected by an interface. In the following explanation the Nitsche method [@Kaltenbacher2010] is presented, because it more robust in practical applications [@huppe2014]. For a detailed deriviation look into [@Kaltenbacher2010] and [@huppe2014].

![a](ncinterfaces.png)

Consequently, non-conforming interfaces allow to use different meshes in the same simulation, as shown in the above figure. Both domains, $\Omega_1$ and $\Omega_2$ exist of different grids and are connected by the non-conform interface $\Gamma_I$. This is especially interesting if different materials are taken into account while a coupled simulation is performed. Furthermore, they are used for simulate rotating structures (for more details look into Features -> Rotating Interfaces) [@schoder2020b].

For defining a non-conform interface first the surfaces, that are part of the interface, can be defined (just to have a better overview, not necessarily needed for the functionality).
Additionally, the non conforming interface must be defined within the domain element. (Arbitrary names are marked as *)
```
  <domain geometryType="*">
    <regionList>
      <region name="*" material="*"/>
    </regionList>
    <surfRegionList>
      <surfRegion name="*" />
      <surfRegion name="*" />
    </surfRegionList>
    <ncInterfaceList>
      <ncInterface name="*" primarySide="*" secondarySide="*"/>
    <ncInterfaceList>
  </domain>
```

The nc-interface must then be declared once again in the respective PDE where it should occur, e.g.,
```
  <pdeList>
    <acoustic formulation="acouPressure">
      <regionList>
        <region name="*"/>
      </regionList>
      <ncInterfaceList>
        <ncInterface name="*" nitscheFactor="100" formulation="Nitsche"/>
      </ncInterfaceList>
    </acoustic>
    ...
  </pdeList>
```
The nitscheFactor is only avaiable for non-conforming interfaces type Nitsche and is a weight for the penalty factor that penalizes the deviation of the solutions and test functions on the two sides $\Omega_1$ and $\Omega_2$. This factor should have no major impact in the range of 20 - 100 [@huppe2014]. A rather high value can lead to a more physical solution but potentially worsens the condition of the equation system. On the other hand, a low nitscheFactor is numerically more stable, but allows for a higher deviation of the solution on the two sides, making the solution less physical.

An alternative coupling method is the classic Mortar FEM, which employs a Lagrange multiplyer to establish the coupling. A Mortar interface can be declared similar to the Nitsche-type Mortar, e.g.,
```
  <pdeList>
    <acoustic formulation="acouPressure">
      <regionList>
        <region name="*"/>
      </regionList>
      <ncInterfaceList>
        <ncInterface name="*" formulation="Mortar"/>
      </ncInterfaceList>
    </acoustic>
  </pdeList>
```

It is also possible to establish an nc-interface between the realms of different PDEs, which can be done in the coupling list, e.g., for acoustic-mechanic coupling,
```
  <couplingList>
      <direct>
        <acouMechDirect>
          <ncInterfaceList>
            <ncInterface name="*"/>
          </ncInterfaceList>
        </acouMechDirect>
      </direct>
    </couplingList>
```


There are several other tags available to the non-conforming interfaces. Please refer to the XSD scheme documentation for further information.

