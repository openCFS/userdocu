# Coils
It is possible to incorporate arbitrarily shaped coils in openCFS as depicted in the following figure.

![coil](general_coil.png){: style="height:300px" : .center}

*arbitrarily shaped coil body $\Omega_\textrm{C}$ with connection ports $\Gamma^+$ and $\Gamma^-$*

>A coil must be specified after `<bcsAndLoads>` in `<coilList>`.


The two connection ports can be prescribed either with

* a current (current driven coil)
* a voltage (voltage driven coil with mandatory serial resistor)

These two types are described in the following. Regarding the geometrical shape of the coil, there is one more special case - the **cylindrical coil**. 

A cylindrical coil is shaped like a hollow cylinder which means that a simplification can be done in openCFS to reduce prior meshing procedures for the coil itself. This simplification can be clearly done since the change in the computed magnetic field can be neglected. The following figure demonstrates such a simplification from a real coil with windings to a coil with a hollow cylinder as conducting material.

![coil](coil.png){: style="height:300px" : .center}

*depiction of  ***(a)*** real cylindrical coil with windings and ***(b)*** substituted coil in openCFS with hollow cylinder as conducting material*

A dedicated current can be prescribed in a current driven coil. It has to be denoted that the corresponding current density $\mathbf{J}$ in a coil equals

$
\mathbf{J} = J \mathbf{e}_J = \frac{I}{\Gamma_\textrm{W}} \mathbf{e}_J = \frac{IN_\textrm{c}}{\kappa\Gamma_\textrm{c}} \mathbf{e}_J
$

Whereas $I$ denotes the prescribed current, $\Gamma_\textrm{W}$ the cross section of **one** winding, $N_\textrm{c}$ the number of turns, $\Gamma_\textrm{c}$ the cross section of the hollow cylinder and $\mathbf{e}_J$ the unit vector in direction of the current flow i.e. the axial direction of the hollow cylinder.

## Coils in 2D
Coils in two dimensional problems can be incorporated by defining the direction of the current in the normal direction of the computation domain (e.g. computation domain equals $xy$-plane and current density of the coil equals $z$-direction).

> It is recommended to prescibe the current density as current value (and use a wireCrossSection of 1). E.g. for a coil with a coil area of `A`, `n` windings and a wire current of `I` specify the current as shown in the 2D example where the direction of the current density is in z direction.

```
<coilList>
    <coil id="coil1"> 
        <source type="current" value="I*n/A"/>
        <part id="1"> 
            <regionList>
                <region name="coilwire"/>
            </regionList>
            <direction>
                <analytic>
                    <comp dof="z" value="1.0"/>
                </analytic>
            </direction>
            <wireCrossSection area ="1"/>
        </part>
    </coil>
</coilList>
```

## Coils in 3D
Prescribing a current in a three dimensional coil might differ from the two dimensional case due to an initially unknown (and changing) direction of the current density field through an arbitrarily shaped object representing a coil and/or missing connection ports of closed three dimensional coils (similar to hollow cylindrical coils).

### Axi-Symmetric Coils
Axi-symmetric configurations can be treated in the same manner as coils in the two dimensional case since the direction of the current density $\mathbf{J} = J \mathbf{e}_\varphi$ is directly given by the $\varphi$-direction.

### Arbitrary Geometry of Coil

#### Coils without Connection Ports

Incorporating a three dimensional hollow (eventually cylindrical) coil is done by first splitting up the coil body into two sub-domains with 4 interfaces, e.g. named `coilSurfXPos`, `coilSurfXNeg` and `coilSurfYPos`, `coilSurfYNeg`. This as shown in the following figure. 

![coil division](coil_cut.png){: style="height:300px" : .center}

*division of an arbitrary three dimensional coil into two sub-domains with respective interfaces*

Now two separate `elecConduction`problems (e.g. with unit voltage excitation) can be solved where the quantity of interest is nothing else than the direction of the `elecCurrentDensity`, i.e. $\mathbf{J}$ in the coil region. In the `<coilList>` afterwards, the direction can be specified directly via `<direction>` as the following listing shows.

````
<direction>
    <external>
         <sequenceStep index="1">
             <quantity name="elecCurrentDensity" pdeName="elecConduction"/>
                 <timeFreqMapping><constant/></timeFreqMapping>
        </sequenceStep>
    </external>
</direction>

````
*A full description of an electric flow problem can be found in [ElecFlowPDE](../../../PDEExplanations/Singlefield/ElectricFlowPDE/README.md)*

#### Coils with Connection Ports
Coils that are preliminarily equipped with connection ports (similar to the figure at the beginning) need not to be cut in two sub-domains as potentials or currents can be described directly at these ports.

## Current Driven Coil
As already depicted in the listings above, a dedicated  current may be prescribed in a coil via `source type="current"`. 

> The value for a current driven coil equals not the current $I$ itself, but the current density, i.e. $\mathbf{J}$.

## Voltage Driven Coil
A presciption of a voltage for a coil is also possible. The source type has to be chosen to `<source type="voltage" value="V"/>`. Prescribing a coil with a dedicated voltage `V` always require a serial resistance between the voltage source and the coil. The value can be prescribed via `<resistance value="R"/>`.

Besides of the required serial resistor, the voltage driven coil is similar to the current driven coil.







