#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 14 10:38:23 2022

@author: alex
"""
import sys
sys.path.append("/home/alex/Documents/Doktorarbeit/PiezoHysteresis/python")

import hdf5_tools as h5
import h5py

import numpy as np

class Container:
    """
    Saves the time result (doFFT = True)
    
    Container for Time domain
    Time domain: [Nt,Elem]
    """
    def __init__(self,Name,NumDOFs, file, region = None, multistep = 1):
        """
        Parameters
        ----------
        Name : String
            Used to obtain the result with hdf5_tools.get_results().
        NumDOFs : Integer
            Should be 1 (NodeResult) or 3 (ElementResult).
        file : string
            Path to file (for hdf5_tools.get_results()).
        Nt : Integer
            Number of points in the time series. Used for the FFT (f2t()) .
        doFFT : Boolean, optional
            Triggers the fft for the obtained frequency results . The default is False.
        region : string, optional
            For what region the result should be obtained. The default is None.
        multistep : string, optional
            For what multistep the result should be obtained. The default is 1.

        """

        self.__NumDOFs = NumDOFs
        self.__file = file
        
            
        if self.__NumDOFs ==1:
            #print("Scalar quantity")
            self.val= h5.get_result(self.__file, Name, step = "all", region = region, multistep = multistep)
            self.coord = h5.get_coordinates(self.__file, region = region)
        elif self.__NumDOFs ==3:
            #print("Vector quantity")
            if len(h5.get_result(self.__file,Name,step="all", region = region, multistep = multistep).shape) == 2:
                # print("Exists only of 1 element")
                # [Nt, x]
                self.x=h5.get_result(self.__file,Name,step="all", region = region, multistep = multistep)[:,0]
                self.y=h5.get_result(self.__file,Name,step="all", region = region, multistep = multistep)[:,1]
                self.z=h5.get_result(self.__file,Name,step="all", region = region, multistep = multistep)[:,2]
                self.coord = h5.get_coordinates(self.__file, region = region)

            else:
                # print("More elements in this region")
                # [Elem, Nt, x]
                self.x=h5.get_result(self.__file,Name,step="all", region = region, multistep = multistep)[:,:,0]
                self.y=h5.get_result(self.__file,Name,step="all", region = region, multistep = multistep)[:,:,1]
                self.z=h5.get_result(self.__file,Name,step="all", region = region, multistep = multistep)[:,:,2]
                self.coord = h5.get_coordinates(self.__file, region = region)

class Result:
    """
    Each Result gets a time-container.
    """
    def __init__(self,Name,NumDOFs, DefinedOn, file, region = None, multistep = 1):
        self.__Name = Name
        self.__NumDOFs = NumDOFs
        self.__DefinedOn = DefinedOn
        self.__file=file
        self.Values = Container(self.__Name, self.__NumDOFs, self.__file, region = region, multistep = multistep)

    def getInfo(self, printInfo=True):
        if printInfo:
            print(f"NumDOFS = {self.__NumDOFs}")
            print(f"Defined on {self.__DefinedOn}")
            print(f"Result[Nt,{self.__DefinedOn}]")
                
        return self.__NumDOFs, self.__DefinedOn

#%% Main class
class cfsResult:
    def __init__(self, filePath , multistep = 1, region = None):
        self.__multistep = multistep
        self.__filePath=filePath
        self.__f=h5py.File(self.__filePath,"r")
        self.__region = region
        self.__getAllRegions()
        self.__readAllResults()
        self.__f.close()
        
    def __getAllRegions(self):
        """
        Checks if only 1 region is in result file, otherwise error occurs
        """
        regionList3d =[]
        if self.__region == None:
            regionList = self.__f["Mesh/Regions"].keys()
        else:
            regionList = [self.__region]
            
        for region in regionList:
            if self.__f[f"Mesh/Regions/{region}"].attrs["Dimension"] == 3:
                regionList3d.append(region)
                
        if len(regionList3d) < 1:
            raise Exception("Please specify at least 1 Volumeregion")
        elif len(regionList3d) > 1:
            raise Exception("No region specified but more than one region present. Please choose 1 region.")
        else:
            self.__region3d = regionList3d[0]
        
    def __readResultInfo(self, result):
        """
        Searches for the Result and returns DOFs and where its definded on
        """
        
        try:
            # This function is need for the visit() function
            # Its kinda confusing but it works.
            def find_Key(Key):
                 """ Find first object with 'foo' anywhere in the name """
                 if result in Key:
                     return Key
                 
            path="Results/"+self.__f["Results"].visit(find_Key)
            NumDOFs = self.__f[path]["NumDOFs"][0]
        
            DefinedOn = self.__f[path]["DefinedOn"][0]
            if DefinedOn == 1:
                DefinedOn="node"
            elif DefinedOn == 4:
                DefinedOn="element"
            
            return NumDOFs, DefinedOn
        except:
            print(f"Result {result} not found. Be careful, its casesensitive!")
            
    def __readAllResults(self):
        """
        This function reads all results and creates for each result a class named like the Result).
        """
        # This function is need for the visit() function
        def find_ResultDescription(Key):
            """ Find first object with 'foo' anywhere in the name  for specified multistep"""
            # print(f"MultiStep_{self.__multistep}/ResultDescription")
            if f"MultiStep_{self.__multistep}/ResultDescription" in Key:
                return Key
        
        # used the h5.visit() function to find all results and store it in a list
        self.__AllResults = [*self.__f[self.__f.visit(find_ResultDescription)]]
        for result in self.__AllResults:
            NumDOFs, DefinedOn = self.__readResultInfo(result)
            tmpClass = Result(result, NumDOFs, DefinedOn, self.__f, region= self.__region3d, multistep = self.__multistep)
            setattr(self, result, tmpClass)
            
            
    def printAllResults(self):
        """
        Lists all results available
        """
        print(self.__AllResults)
        
