# HW5: Induction Heating


### Description
Consider the induction heating setup for a thin steel sheet in the sketch down below. We are only considering the depicted cross section in a quasi 2D-model.
The dimensions of all parts (inductor, steel sheet and air) are already given in the mesh file (don't change it!).
Due to the small thickness of the sheet, we need high frequencies for the excitation current (in the kHz range), whereas the thermal problem has time constants in the seconds-range, which results in severe timescale differences.
Therefore we solve the linear magnetic problem in the harmonic regime and the thermal problem in steady state.

The sheet is made of steel with given material properties in the mat.xml file (don’t change it).

The inductor does not consist of litz wire, it's made of massive copper and for practical applications also eddy currents in the inductor have to be considered, which is neglected in this homework by setting the electric conductivity of copper to zero for the magnetic problem.

The inductor is fed by an alternating current of 1 kA, with varying frequencies.

![sketch](sketch.png){: style="width:500px"}

---------------------------

### Tasks
1. Create input-xml files for the required CFS++ simulations. **(1 Points)**
2. Document your results by answering the questions below. **(12 Points)**
3. Answer the theoretical question. **(1 Points)**

### Hints
- Use [`simulation.xml`](simulation.xml) as a starting point, copy it and adapt it to your needs.
- Use the provided template [`mat.xml`](mat.xml)
- The dimensions of all parts are given in [`model_Air-Water-Sheet-Inductor.msh2`](model_Air-Water-Sheet-Inductor.msh2). Do not change this file.

### Submission
Submit all your input files (`*.jou`, `*.xml`) as well as a concise PDF report of your results. Name the archive according to the format `HWn_eNNNNNNN_Lastname.zip`.

---------------------------

### Initial Harmonic Analysis
Simulate the magnetic problem without considering the thermal problem and an excitation frequency of $f$ = 8500 Hz and plot the vector field of the magnetic flux density.

- __Hint:__ Tick Animate Harmonic/Modal Results in paraview and plot the vector field for the ”mode” quantity of the magnetic flux density at ”virtual time”. **(2 Points)**

### Harmonic Analysis Part 2
Now perform the same simulation as before with $f$ = 500 Hz and again plot the vector field of the magnetic flux density. Which differences can you see and why do they occur? **(2 Points)**

### Magnetic-Thermal Problem 
Take the simulation.xml file as starting point because it already includes the magnetic-thermal problem. In this example, the sheet is moving with a velocity of 0.01667 m/s. Use the same thermal boundary conditions (BC) as in `simulation.xml`. Describe the given BC’s from the xml file in your own words, e.g. *”At the left boundary of the sheet, a Dirichlet BC (temperature) of 20 C$^\circ$ is prescribed"*. Simulate the magnetic-thermal problem for $f \in \{ 1000, 3000, 5000, 7000, 9000 \} $ Hz and plot the temperature in the sheet for the different frequencies in one plot. (__HINT:__ plot over line.)
What are the differences and why do they occur? **(5 Points)**

### Theoretical part
How is the heating power computed for the general case and how can we simplify this term for this very example? **(3 Points)**

---------------------------

You can download the templates linked on this page individually by _right click --> save target as_.
Alternativey, head over to the [git repository](https://gitlab.com/openCFS/userdocu/-/tree/master/docs/Exercises/Multiphysics2) and use the _download button_ on the top right: 
Here is a direct link to the [__zip archive__](https://gitlab.com/openCFS/userdocu/-/archive/master/HW5.zip?path=docs/Exercises/Multiphysics2/5_MagMech).

