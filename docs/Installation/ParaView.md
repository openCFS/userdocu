Installing ParaView and activating the CFSReader Plugin
=======================================================

In order to read openCFS native HDF5 data format (`*.cfs` or `*.h5ref` files) with ParaView you need our **CFSReader plugin**.
The plugin has been included in ParaView (from version 5.12) and needs to be **activated** before it can be used.

Install ParaView
----------------

You can build ParaView from source or install an [official ParaView release](https://www.paraview.org/download/) for your operating system.

**Note:** The plugin is included in ParaView from version 5.12, see the [release notes](https://www.kitware.com//paraview-5-12-0-release-notes). 

Activate CFSReader Plugin
-------------------------

In order to read openCFS result files (`*.cfs` files) with ParaView, one must _activate_ the **CFSReader plugin**.

Follow these steps:

  * Open ParaView and in the menu bar click **Tools > Manage Plugins**
  * Select **CFSReader** and click the **Load Selected** button (you can also activate the **Auto Load** check box to load the plugin on every subsequent program start)
  * Now **.cfs** files can be opened and visualized using ParaView










