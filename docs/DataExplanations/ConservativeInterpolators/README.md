# Conservative Interpolation

The conservative interpolation computest the **R**ight **H**and **S**ide force $f$ (computation of the source $f$ is described in [@schoder2020c]) of an partial differential equation

\begin{equation}
\int_\Omega f \varphi {\mathrm {d}} \Omega \, ,
\end{equation}

using first order nodal FEM to solve the system of equations [@schoder2020a]. If you would like to use the conservative filters for higher oder FEM, please contact [Stefan Schoder](https://online.tugraz.at/tug_online/visitenkarte.show_vcard?pPersonenId=1D95C1A49837524D&pPersonenGruppe=3)

## Theory - Publication
This [publication](https://doi.org/10.1142/S2591728520500322) covers the **theory** of the implemented conservative interpolation filters. When using the conservative filters please provide a **citation** in you publication:

_Schoder, Stefan, et al. "Application limits of conservative source interpolation methods using a low Mach number hybrid aeroacoustic workflow." Journal of Theoretical and Computational Acoustics (2020)._

and provide an **acknowledge the usage** at the end of the publication

_The computational results presented have been achieved [in part] using the software openCFS [Cell Centroid Conservative Interpolation/Cut Volume Cell Conservative Interpolation]._

If you have questions related to the interpolators please contact the corresponding author of the [publication](https://doi.org/10.1142/S2591728520500322). For further development and future publications on the topic, we are happy to colaborate wit your research unit (company or university).


## Simulation setup
During the simulation setup, we will discuss how the variables inside the publication relate to the xml-scheme of the simulation setup. Having this knowledge, it should be able for you to start your own simulation using conservative interpolators. CGNS or ENSIGHT input data is supported.

```
<?xml version="1.0" encoding="UTF-8"?>
<cfsdat ...>
  <pipeline>

    <stepValueDefinition>
      ...
    </stepValueDefinition>
    
    <meshInput id="inputFilter" gridType="fullGrid">
      ...
    </meshInput>
...
    <interpolation type="TYPE" id="interp" inputFilterIds="inputFilter">
      <targetMesh>
        <hdf5 fileName="../mesh.h5"/>
      </targetMesh>
      <singleResult>
        <inputQuantity resultName="..."/>
        <outputQuantity resultName="..."/>
      </singleResult>
      <regions>
        <sourceRegions>
          <region name="..."/>
        </sourceRegions>
        <targetRegions>
          <region name="..."/>
        </targetRegions>
      </regions>
    </interpolation>
...
    <meshOutput id="acousticSources" inputFilterIds="interp">
      ...
    </meshOutput>

  </pipeline>
</cfsdat>
``` 


### Cell Centroid Conservative Interpolation
The cell centroid interpolator is defined by the type variable FieldInterpolation_Conservative_CellCentroid.
```
type="FieldInterpolation_Conservative_CellCentroid"
``` 
Based on the following grafics, the interpolation is carried out:

![Conservative Interpolation Cell Centroid](cfs_caa_2.JPG)

### Cut Volume Cell Conservative Interpolation
The cut volume cell centroid interpolator is defined by the type variable FieldInterpolation_Conservative_CutCell.
```
type="FieldInterpolation_Conservative_CutCell"
``` 
Based on the following grafics, the interpolation is carried out:

![Conservative Interpolation Cut Volume Cell](cfs_caa_1.JPG)

# References
\bibliography
