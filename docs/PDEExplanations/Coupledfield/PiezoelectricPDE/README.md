# Piezoelectric PDE

The [mechanical](../../Singlefield/MechanicPDE/README.md) and [electrical](../../Singlefield/ElectrostaticPDE/README.md) partial differential equation are directly coupled (volume-coupling) via the material law. 
## Coupling via linear constitutive law
The consitutive relations can be written in serveral, related forms (both formulations are given here in Voigt-notation). In the d-form,
\begin{equation}
\mathbf{s}=\mathbf{S}\mathbf{\sigma} + \mathbf{d} \mathbf{E}
\mathrm{,}
\end{equation}
\begin{equation}
\mathbf{D}=\mathbf{d} \mathbf{\sigma} + \mathbf{\epsilon^{\sigma}} \mathbf{E}
\mathrm{,}
\end{equation}
and in the e-from,
\begin{equation}
\label{MLM}
\mathbf{\sigma}=\mathbf{C} \mathbf{s} - \mathbf{e} \mathbf{E}
\mathrm{,}
\end{equation}
\begin{equation}
\label{MLE}
\mathbf{D}=\mathbf{e} \mathbf{s} + \mathbf{\epsilon^{s}} \mathbf{E}
\mathrm{.}
\end{equation}
The strain is denoted as $\mathbf{s}$, while $\mathbf{\sigma}$ is the stress tensor (both in Voigt-notation). The electric field is $\mathbf{E}$ and the electric flux density is $\mathbf{D}$.

In the finite-element-methode the $\mathbf{e}$-formulation is used. The piezoelectric coupling tensor $\mathbf{e}$ looks usually as follows,

\begin{equation}
\mathbf{e}=
\left(
\begin{array}{rrrrrr}
0 & 0 & 0 & 0 & e_{\mathrm{15}} & 0 \\
0 & 0 & 0 & e_{\mathrm{15}} & 0 & 0 \\
e_{\mathrm{31}} & e_{\mathrm{31}} & e_{\mathrm{33}} &0 &0 &0\\
\end{array}
\right)
\mathrm{.}
\end{equation}

Both formulations are related, the stiffness tensor $\mathbf{C}$ (for piezoceramics usually transversally isotropic) is the inverse of the compliance tensor $\mathbf{S}$,
\begin{equation}
\mathbf{C} = \mathbf{S}^{-1}
\mathbf{,}
\end{equation}
and the relation between the $\mathbf{d}$-tensor and $\mathbf{e}$-tensor is given by,
\begin{equation}
\mathbf{d}=\mathbf{e} \mathbf{C}^{-1}
\mathrm{.}
\end{equation}
The permittivity tensor at constant strain $\mathbf{\epsilon}^\sigma$ can be calculated out of the permittivity tensor at constant strain $\mathbf{\epsilon}^s$,
\begin{equation}
\mathbf{\epsilon}^\sigma= \mathbf{\epsilon}^s + \mathbf{e} \mathbf{C}^{-1} \mathbf{e}^{\mathrm{T}}
\mathrm{.}
\end{equation}
## Governing equations
If we insert eq. \ref{MLM} into the [mechanical PDE](../../Singlefield/MechanicPDE/README.md) and eq. \ref{MLE} into the [electrical PDE](../../Singlefield/ElectrostaticPDE/README.md), while introducing the differential operator $\mathcal{B}$,

\begin{equation}
\mathcal{B}=
\left(
\begin{array}{rrrrrr}
\frac{\partial}{\partial x} 	& 0 	& 0 	& 0 	& \frac{\partial}{\partial z}	& \frac{\partial}{\partial y}	\\
0 	& \frac{\partial}{\partial y}	& 0	& \frac{\partial}{\partial z}	& 0	& \frac{\partial}{\partial x}	\\
0	& 0	& \frac{\partial}{\partial z}	& \frac{\partial}{\partial y}	& \frac{\partial}{\partial x}	& 0	\\
\end{array}
\right)
\mathrm{,}
\end{equation}

using the linearised strain-displacement relation,
\begin{equation}
\mathbf{S}=\mathcal{B} \mathbf{u}
\mathrm{,}
\end{equation}
and the definition of the electric potential
\begin{equation}
\mathbf{E} = - \nabla \phi
\mathrm{,}
\end{equation}
we arrive at the coupled mechanical (\ref{CPDEM}) and electrical (\ref{CPDEE}) PDE in their strong formulation,

\begin{equation}
\label{CPDEM}
\rho \mathbf{\ddot{u}} - \mathcal{B}^T \left( \mathbf{C} \mathcal{B} \mathbf{u} - \mathbf{e}^T \nabla \phi   \right) 
=
\mathbf{\displaystyle{f}}_{\mathrm{V}}
\mathrm{,}
\end{equation}

\begin{equation}
\label{CPDEE}
\nabla \cdot \left( 
\mathbf{e} \mathcal{B} \mathbf{u} - \mathbf{\epsilon} \nabla \phi
\right)=
q_{\mathrm{e}}
\mathrm{.}
\end{equation}

##Analysis Types
There are different analysis types availible for piezoelectric simulations:

**Static analysis:** $\frac{\partial}{\partial t} = 0$

	<analysis>
		<static/>
	</analysis>

**Transient analysis:**  $\frac{\partial}{\partial t} \neq 0$

	<analysis>
		<transient>
			<numSteps>100</numSteps>
			<deltaT>3e-06</deltaT>
		</transient>
	</analysis>
      	
**Harmonic analysis: **  $\frac{\partial}{\partial t}(\cdot) = j\omega (\cdot)$

   	<analysis>
       	<harmonic>
         		<frequencyList>
         			<freq value="100"/>
         			<freq value="200"/>
         			<freq value="300"/>
         		<\frequencyList>
		<\harmonic>
	<\analysis>	
	
**Eigenfrequenzy analysis:**  $\frac{\partial}{\partial t}(\cdot) = j\omega (\cdot)$ and $RHS =0$

	<analysis>
      		<eigenFrequency>
        		<isQuadratic>no</isQuadratic>
        		<numModes>10</numModes>
        		<freqShift>0</freqShift>
        		<writeModes normalization="max">yes</writeModes>
        		<!-- scale them to a maximum displacement of 1 -->
      		</eigenFrequency>
    	</analysis>

## PDE types
Within the command <pdeList> we define the induvidual PDEs with their boundary conditions, etc.
	<pdeList>
		<mechanic subType="3d">
			...
		</mechanic>
		<electrostatic>
			...
		</electrostatic>
	</pdeList>

## Defining the coupling
The mechanical and electrostatic PDE are coupled directly over the volume. This is done (in the XML-file) after the `pdeList`

	...
	</pdeList>
	<couplingList>
		<direct>
			<piezoDirect>
				<regionList>
					<region name="V"/>
				</regionList>
			</piezoDirect>
		</direct>
	</couplingList>
	...
        
## Boundary conditions and Loads
For the piezoelctric coupled system the boundary conditions from the [mechanics](../../Singlefield/MechanicPDE/README.md) and the [electrostatics](../../Singlefield/ElectrostaticPDE/README.md) PDE can be applied. The boundary conditions and loads can be defined for each PDE.

## Material
Additionally to the [mechanical](../../Singlefield/MechanicPDE/README.md) and [electrical](../../Singlefield/ElectrostaticPDE/README.md) properties, the couple tensor in e-form must be given.

     <piezo>
      <piezoCoupling>
        <linear>
          <tensor dim1="3" dim2="6">
            <real>
          0.0000e-003  0.0000e-003  0.0000e-003  0.0000e-003 12.7174e+000 0.0000e-003
          0.0000e-003  0.0000e-003  0.0000e-003 12.7174e+000  0.0000e-003 0.0000e-003
         -5.1714e+000 -5.1714e+000 15.1005e+000  0.0000e-003  0.0000e-003 0.0000e-003
        </real>
          </tensor>
        </linear>
      </piezoCoupling>
    </piezo>
    
An example material file is availible under tutorials for [Piezoelectric Unit-Cube](../../../Tutorials/Piezo_UnitCube/README.md).

The polarization direction inside the material can be rotated in respect to the global coordinate system (default is $\alpha=\beta=\gamma=0$).
<img src="kardan-rot2.png" width="350">

    <domain geometryType="3d">
        <regionList>
            <region name="V" material="PZT-4">
                <matRotation alpha="0.0" beta="0" gamma="0"/>
            </region>
        </regionList>
    </domain>
## Postprocessing results
The typical postprocessing results for the [mechanical](../../Singlefield/MechanicPDE/README.md) and [electrical](../../Singlefield/ElectrostaticPDE/README.md) PDE can be defined in each PDE.

## Modelling assumptions
There a certain modelling assumptions made:

* Small strains
* Linear piezoelectricity
* Perfect, homogeneous polarization
* Zero resistance assumed in electrodes
* Electrodes are often assumed mechanically not significant (infinitely thin)

## Tutorials
[Piezoelectric Unit-Cube](../../../Tutorials/Piezo_UnitCube)
## Applications
Coming soon...
