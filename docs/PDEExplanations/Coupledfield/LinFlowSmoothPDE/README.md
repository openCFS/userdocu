# LinFlowSmoothPDE

## LinFlow-Smooth coupling
### Coupling conditions

In LinFlow-smooth coupled problems, the following transmission condition along the interfaces has to be fulfilled:

* **Kinematic coupling condition** is velocity continuity at the interface 
\begin{equation}
\frac{\partial \mathbf u}{\partial t} = \mathbf v \, ,
\label{eq:veltranscond-mechcoupl}
\end{equation} 

where $\mathbf u$ and $\mathbf v$ are mechanic displacement and LinFlow velocity vector, respectively. 

### Formulation for moving domains
For moving domains, the equations of the LinFlowPDE are extended by terms containing the grid velocity. The linearized conservation equations in ALE-formulation for the conservation of mass and momentum are 

\begin{eqnarray}
\frac{1}{c^2} \frac{\partial p}{\partial t} +\mathbf \nabla \cdot (\rho_0 \mathbf v) - \frac{1}{c^2} \mathbf v_g \cdot \mathbf \nabla p = 0 \label{eq:massAcou}\,,\\ \rho_0 \frac{\partial \mathbf v}{\partial t} - \rho_0 \mathbf v_g \cdot  \mathbf \nabla \mathbf v = \mathbf \nabla \cdot \mathbf \sigma \label{eq:momentumAcou}.
\end{eqnarray}

The fluid stress tensor $\sigma$ remains the same as in the LinFlow PDE: 

\begin{equation}
\mathbf \sigma =  -p \mathbf I + \mu \big (\mathbf \nabla \mathbf v+(\mathbf \nabla \mathbf v)^T \big) +(\lambda - \frac{2}{3} \mu) (\mathbf \nabla \cdot \mathbf v)\mathbf I \, .
\end{equation}

In these equations $p$ denotes the perturbed fluid pressure, $\rho_0$ the mean density, $c$ the speed of sound, $\mathbf v$ the perturbed fluid velocity and $\mathbf v_g$ the grid velocity. Furthermore, $\mu$ is the dynamic viscosity of the fluid and $\mathbf I$ represents the identity tensor. 


To derive the weak forms, the strong forms of the conservation of mass \eqref{eq:massAcou} and momentum \eqref{eq:momentumAcou} are multiplied by appropriate test functions and integrated over the whole computational moving domain $\Omega (t)$. 

\begin{eqnarray}
\int\limits_{\Omega (t)} \big(p' \frac{1}{c^2} \frac{\partial p}{\partial t} + p' \mathbf \nabla \cdot (\rho_0 \mathbf v) - p' \frac{1}{c^2}\mathbf v_g \cdot \mathbf \nabla p \big) {\mathrm {d}} \Omega = 0,
\end{eqnarray}

\begin{equation}
\int\limits_{\Omega (t)} \big(\rho_0 \mathbf v' \cdot \frac{\partial \mathbf v}{\partial t} - \rho_0 \mathbf v' \cdot (\mathbf v_g \cdot \mathbf \nabla \mathbf v ) \big) \, {\mathrm {d}} \Omega + 
\int\limits_{\Omega (t)} \big( \mathbf \nabla \mathbf v' : \sigma \big) {\mathrm {d}} \Omega =  \int\limits_{\Gamma (t)} (\mathbf v' \cdot \sigma) \cdot \mathbf n {\mathrm {d}} \Gamma .
\label{eq:thermoviscous-momentum_weak}
\end{equation}

In these equations, $\mathbf v'$ and $ p’$ are fluid velocity and pressure test functions, respectively.
The vector $\mathbf n$ is the surface normal vector, and $\Gamma$ is the boundary surface.
To meet the inf-sup condition, it is recommended that the shape function for velocity is a one-order higher polynomial than that for the pressure. 

To dedicate these orders in the simulation-xml file, tags ``` presPolyId ``` for pressure and ``` velPolyId ``` for velocity polynomial orders are defined.

```
<fluidMechLin  formulation = "" presPolyId = "" velPolyId = "">
```

The grid velocity terms have to be enabled in the xml-simulation file as follows (they are activated by default):
```
<fluidMechLin  formulation = "" presPolyId = "" velPolyId = "" enableGridVelC1="true" enableGridVelC2="true">
```

$C_1$ enables the grid velocity term in the conservation of mass equation and $C_2$ enables the grid velocity term in the conservation of momentum equation. The factors $C_1$ and $C_2$ are activated by setting their value to 1 and deactivated by setting their value to 0: 

\begin{eqnarray}
\frac{1}{c^2} \frac{\partial p}{\partial t} +\mathbf \nabla \cdot (\rho_0 \mathbf v) - C_1 \cdot \frac{1}{c^2} \mathbf v_g \cdot \mathbf \nabla p = 0 \, ,\\ 
\rho_0 \frac{\partial \mathbf v}{\partial t} - C_2 \cdot \rho_0 \mathbf v_g \cdot  \mathbf \nabla \mathbf v = \mathbf \nabla \cdot \mathbf \sigma.
\end{eqnarray}




### Analysis Types
The coupling can be solved in transient studies.


* **Transient:** $\frac{\partial }{\partial t}\neq 0$

```
    <analysis>
      <transient>
        <numSteps>10</numSteps>
        <deltaT>0.1</deltaT>
      </transient>
    </analysis>
```


### Defining the coupling 

The smooth-LinFlow coupling is a forward coupling using the 'smoothVelocity' as coupling quantity. The coupling and all calculations are done iteratively until we get a convergent solution (stopping criteria defined in couplingList).

In the fluidMechLin PDE a movingMeshList has to be defined, since we have a moving domain. 

```
<movingMeshList>
    <movingMesh name="moveGridID">
        <coupling pdeName="smooth">
            <quantity name="smoothVelocity"/>
        </coupling>
    </movingMesh>
</movingMeshList>
``` 

 Furthermore, the forward coupling of the velocity has to be defined not just on the inside of the domain but also on its (moving) boundary. Hence, the forward-coupled velocity in the fluidMechLin is defined under the ```<bcsAndLoads>``` tag as follows: 
```
<velocity name="IF_name">
  <coupling pdeName="smooth">
    <quantity name="smoothVelocity"/>
  </coupling>
</velocity>
```

The [Smooth PDE](../../Singlefield/SmoothPDE/README.md) is used for the calculation of the deformed mesh as well as the grid velocity (a quasi static solution of an artificial mechanical problem). 

Within the ```<couplingList>``` tag in the xml-simulation file the iterative coupling can be defined as:
```
<couplingList>
  <iterative PDEorder="">
    <convergence logging="yes" maxNumIters="5" stopOnDivergence="no">
      <quantity name="smoothDisplacement" value="1e-3" normType="rel"/>
      <quantity name="smoothVelocity" value="1e-3" normType="rel"/>
      <quantity name="fluidMechVelocity" value="1e-3" normType="rel"/>
      <quantity name="fluidMechPressure" value="1e-3" normType="rel"/>
    </convergence>
    <geometryUpdate>
      <region name="Region_name"/>
    </geometryUpdate>
  </iterative>
</couplingList>
```

In the couplingList the order in which the PDEs are calculated can be specified. With regard to smooth-LinFlow coupling problems we want to calculate the smoothPDE in the first step, therefore we can specify the PDEorder as follows:
```
<couplingList>
  <iterative PDEorder="smooth">
``` 

### Material and Postprocessing results

In the material-xml file a flow material input for the [LinFlow PDE](../../Singlefield/LinFlowPDE/README.md) as well as a mechanical (pseudo) material for the mesh movement goverend by the [Smooth PDE](../../Singlefield/SmoothPDE/README.md) should be defined. 

The postprocessing results and the boundary conditions for each PDE are described in the "Singlefield" section. 
In addition, we can compute the following element result which is defined in the LinFlow section (for performance reasons elemResult rather than nodeResult): 
```
<elemResult type="fluidMechMeshVelocityElem">
  <allRegions outputIds="h5"/>
</elemResult>
```
