# Smooth PDE


## Governing Equations

The SmoothPDE can be described as a quasi static mechanic. Hence, the difference to the governing equation of the [Mechanic PDE](../../Singlefield/MechanicPDE/README.md) is that the SmoothPDE is not time dependent nor under the influence of external forces, therefore $\frac{\partial^2 \rho \mathbf{u}}{\partial t^2}=0$ and $\mathbf{g}=0$ at the force density $\mathbf{f}=\nabla \cdot \sigma + \mathbf{g}$. With these assumptions, we obtain 
\begin{equation}
\nabla \cdot \mathbf{\sigma} = 0 \label{eq:govSmooth},
\end{equation}
where $\mathbf{\sigma}$ is the stress tensor (rank 2). It has to be noted that $\mathbf{f}$ is a body force density
which is exerted from the outside. We assume linear, elastic material behaviour which may be anisotropic and express it via Hooks Law which is given by
\begin{equation}
\mathbf{\sigma} = \mathbf{C} : \mathbf{s}, \label{eq:stresstensor}
\end{equation}
where $\mathbf{C}$ denotes the stiffness tensor (rank 4) and $\mathbf{s}$ the strain tensor (rank 2). Furthermore, we assume small strains in order to use the linearized strain displacement relationship given as
\begin{equation}
\mathbf{s} = \frac{1}{2} ( \nabla\mathbf{u} + (\nabla\mathbf{u})^{\textrm{T}}). \label{eq:straintensor}
\end{equation}
Multiplying \eqref{eq:govSmooth} with a test function $\mathbf{u}^\prime$ and inserting \eqref{eq:stresstensor} and \eqref{eq:straintensor} finally leads to the weak form of the PDE given by
\begin{equation}
\frac{1}{2} \int_{\Omega} (\nabla \mathbf{u}^\prime) : \mathbf{C} : \left( \nabla \mathbf{u} + (\nabla \mathbf{u})^{\textrm{T}} \right) \mathrm{d}\Omega - \int_{\Gamma} (\mathbf{u}^\prime \cdot \mathbf{\sigma}) \cdot \mathbf{n} \mathrm{d}\Gamma~=~0.
\end{equation}

## Boundary conditions

The boundary conditions can be given as

\begin{equation}
\mathbf{u}(t) = \mathbf{u}_{\textrm e}(t) \qquad \textrm{on } \Gamma_{\textrm e},
\end{equation}

\begin{equation}
\mathbf{\sigma}(t)\cdot\mathbf{n} = \mathbf{\sigma}_{\textrm n}(t) \qquad \textrm{on } \Gamma_{\textrm n},
\end{equation}

with $\mathbf{u}_{\textrm e}$ as the Dirichlet (essential) BC-value, $\Gamma_{\textrm e}$ the essential boundary, $\mathbf{n}$ the normal vector on the Neumann (natural) boundary $\Gamma_{\textrm n}$ and $\mathbf{\sigma}_{\textrm n}$ the traction at the boundary. Please note that all surfaces that are not manually defined will automatically be free of traction. The boundary conditions can be defined in the xml-file within the `<bcsAndLoads>` tag.

```
        <bcsAndLoads>
          <fix name="">
            <comp dof=""/>
          </fix>
          <displacement name="">
            <comp dof="" value=""/>
          </displacement>
        </bcsAndLoads>
```

* **fix**: Fixes the nodes of the given geometry where only the direction (e.g. "x", "y", "z") specified by the tag `<comp dof ="">` is locked. This allows to fix the geometry in one or more directions (homogeneous Dirichlet value $\mathbf{u} = \mathbf{0}$).
* **displacement**: Similar to `<fix>`, but instead of setting the value of the displacement to zero a certain value can be specified (inhomogeneous Dirichlet value $\mathbf{u} = \mathbf{u}_{\textrm e}$).


## Material

In the general case the material used for the simulation is anisotropic which can be defined in the material-xml as (same as in the [Mechanic PDE](../../Singlefield/MechanicPDE/README.md)). Due to the anisotropy, one can influence and control the grid deformation and thus the element quality.

```
  <material name="material2">
    <smooth>
      <density>
        <linear>
          <real> 1.0e+3 </real>
        </linear>
      </density>
      <elasticity>
        <linear>
          <tensor dim1="6" dim2="6">
            <real>
            1.34615E+03 5.76923E+02 5.76923E+02 0.00000E+00 0.00000E+00 0.00000E+00
            5.76923E+02 1.34615E+03 5.76923E+02 0.00000E+00 0.00000E+00 0.00000E+00
            5.76923E+02 5.76923E+02 1.34615E+03 0.00000E+00 0.00000E+00 0.00000E+00
            0.00000E+00 0.00000E+00 0.00000E+00 3.84615E+02 0.00000E+00 0.00000E+00
            0.00000E+00 0.00000E+00 0.00000E+00 0.00000E+00 3.84615E+02 0.00000E+00
            0.00000E+00 0.00000E+00 0.00000E+00 0.00000E+00 0.00000E+00 3.84615E+02
          </real>
          </tensor>
        </linear>
      </elasticity>
    </smooth>
  </material>
```

For this example, all 21 individual components of the elasticity tensor can be defined. For a simpler case, e.g. an isotropic material, the definition of the elasticity reduces to
```
<elasticity>
  <linear>
    <isotropic>
      <elasticityModulus>
        <real> 3e7 </real>
      </elasticityModulus>
      <poissonNumber>
        <real> 0.2 </real>
      </poissonNumber>
    </isotropic>
  </linear>
</elasticity>
```

where the stiffness tensor is defined via 

\begin{equation}
	\mathbf{C} = \begin{bmatrix}
	2 \mu + \lambda & \lambda & \lambda & 0 & 0 & 0\\
	\lambda & 2 \mu + \lambda & \lambda & 0 & 0 & 0\\
	\lambda & \lambda & 2 \mu + \lambda & 0 & 0 & 0\\
	0 & 0 & 0 & \mu & 0 & 0\\
	0 & 0 & 0 & 0 & \mu & 0\\
	0 & 0 & 0 & 0 & 0 & \mu
	\end{bmatrix}.
\end{equation}

Where $\mu$ and $\lambda$ are the Lam\'{e} constants given by

\begin{equation}
\mu = \frac{E}{2(1+\nu)}
\end{equation}

\begin{equation}
\lambda = \frac{E \nu}{(1+\nu)(1-2\nu)}
\end{equation}

has been used. In these equations, $E$ denotes the elasticity modulus and $\nu$ the poisson ration.



## Analysis Types
As analysis type the transient analysis can be used

* **Transient:** $\frac{\partial }{\partial t}\neq 0$

```
    <analysis>
      <transient>
        <numSteps>10</numSteps>
        <deltaT>0.1</deltaT>
      </transient>
    </analysis>
```


## Postprocessing Results 

#### Node Results

* **Smooth Displacement:** Displacement vector for each node. (primary solution)
```
<nodeResult type="smoothDisplacement">
```    

* **Smooth Velocity:** Velocity vector for each node.
\begin{equation}
\mathbf{v} = \frac{\partial \mathbf{u}}{\partial t}
\end{equation}
```
<nodeResult type="smoothVelocity">
```

* **Smooth Acceleration:** Acceleration vector for each node.
\begin{equation}
\mathbf{a} = \frac{\partial^2 \mathbf{u}}{\partial t^2}
\end{equation}
```
<nodeResult type="smoothAcceleration">
```

* **Smooth ZeroStress:** nodal zeroStress
The SmoothZeroStress is for test purposes to define a fictive back coupling which does not have any influence.
```
<nodeResult type="smoothZeroStress">
```
