<?xml version="1.0" encoding="UTF-8"?>
<cfsSimulation xmlns="http://www.cfs++.org/simulation" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.cfs++.org/simulation file:/share/programs/cfs_release/CFS-UE2021S-Linux/share/xml/CFS-Simulation/CFS.xsd">
  <documentation>
    <title></title>
    <authors>
      <author>jdyduch</author>
    </authors>
    <date></date>
    <keywords>
      <keyword>magneto-mechanic</keyword>
    </keywords>
    <references></references>
    <isVerified>no</isVerified>
    <description>
      This example shows how a grid adaptation with mesh smoothing PDE can be used in a coupled magnetic-mechanic problem.
    </description>
  </documentation>
  
  <fileFormats>
    <input>
      <cdb fileName="magnet_coil_mesh.cdb"/>
    </input>
    <output>
      <hdf5/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>

  <domain geometryType="3d"> <!-- 3D geometry necessary to use magneticEdge formulation -->
    <regionList>
      <region name="V_Air_up" material="air"/>
      <region name="V_Air_down" material="air"/>
      <region name="V_Coil" material="copper"/>
      <region name="V_Core" material="steel"/>
      <region name="V_Magnet" material="steel"/>
    </regionList>
  </domain>
  
  <!--Static simulation-->

  <sequenceStep index="1">
    <analysis>
      <static/>
    </analysis>

    <pdeList>
    <magneticEdge>
        <regionList>
          <region name="V_Air_up"/>
          <region name="V_Air_down"/>
          <region name="V_Coil"/>
          <region name="V_Core"/>
          <region name="V_Magnet"/>
        </regionList>

      <bcsAndLoads>
        <fluxParallel name="S_Flux_x"/>
        <fluxParallel name="S_Flux_y"/>
        <fluxParallel name="S_Flux_front"/>
        <fluxParallel name="S_Flux_back"/>
        
        <fluxDensity name="V_Magnet">
          <comp dof="x" value="0"/>
          <comp dof="y" value="-1.5"/>
          <comp dof="z" value="0"/>
        </fluxDensity>
        
      </bcsAndLoads> 
      <coilList>
        <coil id="myCoil1">
          <source type="current" value="1e+06"/> 
          <part id="1">
            <regionList>
              <region name="V_Coil"/>
            </regionList>
            <direction>
              <analytic>
                <comp dof="z" value="1"/>
              </analytic>
            </direction>
            <wireCrossSection area="1"/>
            <resistance value="0"/> 
          </part>
        </coil>
      </coilList>
      
      <storeResults>
        <elemResult type="magPotential">
          <allRegions/>
        </elemResult>
        <elemResult type="magForceLorentzDensity">
          <allRegions/>
        </elemResult>
        <elemResult type="magFluxDensity">
          <allRegions/>
        </elemResult>
        <elemResult type="magTotalCurrentDensity">
          <allRegions/>
        </elemResult>
        <regionResult type="magForceLorentz">
          <allRegions/>
        </regionResult>
      </storeResults>
    </magneticEdge>
      
      <mechanic subType="3d">
        <regionList>
          <region name="V_Coil" />
          <region name="V_Core" />
        </regionList>
        <bcsAndLoads>
          <fix name="S_fix_xz">
            <comp dof="x"/>
            <comp dof="z"/>
          </fix>
          <concentratedElem name="P_Spring" dof="y" stiffnessValue="1e+03"/>
          <forceDensity name="V_Coil">
            <coupling pdeName="magneticEdge">
              <quantity name="magForceLorentzDensity"/>
            </coupling>
          </forceDensity> 
          
        </bcsAndLoads>
        <storeResults>
          <nodeResult type="mechDisplacement">
            <allRegions/>
          </nodeResult>
        </storeResults>
      </mechanic>
      
     <smooth subType="3d">
        <regionList>
          <region name="V_Air_up"/>
          <region name="V_Air_down"/>
        </regionList>
        
        <bcsAndLoads>
          <displacement name="S_Smooth_c">
            <coupling pdeName="mechanic">
              <quantity name="mechDisplacement"/>
            </coupling>
          </displacement>
          <fix name="S_Smooth_fix">
            <comp dof="x"/>
            <comp dof="y"/>
            <comp dof="z"/>
          </fix> 
          <fix name="S_smooth_fix_xz">
            <comp dof="x"/>
            <comp dof="z"/>
          </fix> 
        </bcsAndLoads> 
        
        <storeResults>
          <nodeResult type="smoothDisplacement">
            <allRegions/>
          </nodeResult>
          <nodeResult type="smoothVelocity">
            <allRegions/>
          </nodeResult>
        </storeResults>
      </smooth>  
    </pdeList>
    
    <couplingList>
      <iterative>
        <convergence logging="yes" maxNumIters="20" stopOnDivergence="yes">
          <quantity name="mechDisplacement" value="1e-6" normType="rel"/>
          <quantity name="magForceLorentz" value="1e-6" normType="rel"/>
          <quantity name="smoothDisplacement" value="1e-6" normType="rel"/>
        </convergence>
        <!-- Define geometric update -->
        <geometryUpdate>
          <region name="V_Coil"/>
          <region name="V_Core"/>
          <region name="V_Air_up"/>
          <region name="V_Air_down"/>
        </geometryUpdate>
      </iterative>
    </couplingList>
    
 </sequenceStep>
  
  <!--Transient simulation-->
  
  <sequenceStep index="2"> 
    <analysis>
      <transient>
        <numSteps>    1500  </numSteps>
        <deltaT>     7e-03 </deltaT>
      </transient> 
    </analysis>
    
    <pdeList>
      <magneticEdge  systemId="mag">
        <regionList>
          <region name="V_Air_up"/>
          <region name="V_Air_down"/>
          <region name="V_Coil"/>
          <region name="V_Core"/>
          <region name="V_Magnet"/>
        </regionList>
        
        <bcsAndLoads>
          <fluxParallel name="S_Flux_x"/>
          <fluxParallel name="S_Flux_y"/>
          <fluxParallel name="S_Flux_front"/>
          <fluxParallel name="S_Flux_back"/>
          
          
          <fluxDensity name="V_Magnet">
            <comp dof="x" value="0"/>
            <comp dof="y" value="-1.5"/>
            <comp dof="z" value="0"/>
          </fluxDensity>
          
        </bcsAndLoads> 
        <coilList>
          <coil id="myCoil1">
            <source type="current" value="2e+06*sin(2*pi*5*t)"/> 
            <part id="1">
              <regionList>
                <region name="V_Coil"/>
              </regionList>
              <direction>
                <analytic>
                  <comp dof="z" value="1"/>
                </analytic>
              </direction>
              <wireCrossSection area="1"/>
              <resistance value="0"/> 
            </part>
          </coil>
        </coilList>
        
        <storeResults>
          <elemResult type="magPotential">
            <allRegions/>
          </elemResult>
          <elemResult type="magForceLorentzDensity">
            <allRegions/>
          </elemResult>
          <elemResult type="magFluxDensity">
            <allRegions/>
          </elemResult>
          <elemResult type="magTotalCurrentDensity">
            <allRegions/>
          </elemResult>
          <regionResult type="magForceLorentz">
            <allRegions/>
          </regionResult>
        </storeResults>
      </magneticEdge>
      
      <mechanic subType="3d" systemId="mech">
        <regionList>
          <region name="V_Coil" />
          <region name="V_Core" />
        </regionList>
        <bcsAndLoads>
          <fix name="S_fix_xz">
            <comp dof="x"/>
            <comp dof="z"/>
          </fix>
          <concentratedElem name="P_Spring" dof="y" stiffnessValue="1e+03" dampingValue="9"/>
          <forceDensity name="V_Coil">
            <coupling pdeName="magneticEdge">
              <quantity name="magForceLorentzDensity"/>
            </coupling>
          </forceDensity> 
          
        </bcsAndLoads>
        <storeResults>
          <nodeResult type="mechDisplacement">
            <allRegions/>
          </nodeResult>
        </storeResults>
      </mechanic>
      
      <smooth subType="3d">
        <regionList>
          <region name="V_Air_up"/>
          <region name="V_Air_down"/>
        </regionList>
        
        <bcsAndLoads>
          <displacement name="S_Smooth_c">
            <coupling pdeName="mechanic">
              <quantity name="mechDisplacement"/>
            </coupling>
          </displacement>
          <fix name="S_Smooth_fix">
            <comp dof="x"/>
            <comp dof="y"/>
            <comp dof="z"/>
          </fix> 
          <fix name="S_smooth_fix_xz">
            <comp dof="x"/>
            <comp dof="z"/>
          </fix> 
        </bcsAndLoads> 
        
        <storeResults>
          <nodeResult type="smoothDisplacement">
            <allRegions/>
          </nodeResult>
          <nodeResult type="smoothVelocity">
            <allRegions/>
          </nodeResult>
        </storeResults>
      </smooth>  
    </pdeList>
    
    <couplingList>
      <iterative>
        <convergence logging="yes" maxNumIters="20" stopOnDivergence="yes">
          <quantity name="mechDisplacement" value="1e-6" normType="rel"/>
          <quantity name="magForceLorentz" value="1e-6" normType="rel"/>
          <quantity name="smoothDisplacement" value="1e-6" normType="rel"/>
        </convergence>
        <!-- Define geometric update -->
        <geometryUpdate>
          <region name="V_Coil"/>
          <region name="V_Core"/>
          <region name="V_Air_up"/>
          <region name="V_Air_down"/>
        </geometryUpdate>
      </iterative>
    </couplingList>
    
  </sequenceStep>

</cfsSimulation>
