from scipy.io import mmread, mmwrite
import scipy.sparse
import numpy as np
import sys

# read in the arguments 
AFileName = sys.argv[1]
BFileName = sys.argv[2]
arg_sigma = complex(sys.argv[3])
arg_k = int(sys.argv[4])
arg_tol = sys.argv[5]

# import system matrices
A = mmread(AFileName)
B = mmread(BFileName)

# compute eigenvalues and corresponding eigenvectors
eigenvalues, eigenvectors = scipy.sparse.linalg.eigs(A,k=arg_k,M=B,sigma=arg_sigma,tol=arg_tol)

# reshape the result arrays so they have only 1 column
eigenvalues = eigenvalues[np.newaxis].T
eigenvectors = eigenvectors.reshape((-1,1), order='F')

# recast and save
mmwrite(sys.argv[6], scipy.sparse.coo_matrix(eigenvalues))
mmwrite(sys.argv[7], scipy.sparse.coo_matrix(eigenvectors))

