#!/bin/bash -i

path_gmsh=gmsh
path_openCFS=cfs

mkdir -p -- "XML_files"
mkdir -p -- "Arrays"

$path_gmsh geometry_incoming.geo -2 -v 0 -format msh2 -o geometry_incoming.msh
$path_gmsh geometry_discs.geo -2 -v 0 -format msh2 -o geometry_discs.msh
$path_gmsh geometry_tilted_slits.geo -2 -v 0 -format msh2 -o geometry_tilted_slits.msh

for angle_s in '60' '30' '00' 'm30' 'm60' ;
do
  angle='1'
  echo Simulationon $angle_s
  if [ $angle_s = '60' ]; then
    angle='60'
    k='3'
  elif [ $angle_s = '30' ]; then
    angle='30'
    k='2'
  elif [ $angle_s = 'm30' ];then
    angle='-30'
    k='2'
  elif [ $angle_s = 'm60' ]; then
    angle='-60'
    k='3'
  elif [ $angle_s = '00' ]; then
    angle='0'
    k='1'
  fi
  
  job_inc=Inc_$angle_s
  job_full=Discs_$angle_s
  job_full_new=Tilted_Slits_$angle_s


  
# Replace Angle, k-Factor, Angle in Path, Meshfile-name and
  sed 's/angle_s/'$angle'/g; s/k_s/'$k'/g; s/angle_path/'$angle_s'/g' incoming_simulation.xml > 'XML_files/'$job_inc'.xml'
  sed 's/angle_s/'$angle'/g; s/k_s/'$k'/g; s/Path_str/'$job_full'/g; s/mesh_name/'geometry_discs.msh'/g' full_simulation.xml > 'XML_files/'$job_full'.xml'
  sed 's/angle_s/'$angle'/g; s/k_s/'$k'/g; s/Path_str/'$job_full_new'/g; s/mesh_name/'geometry_tilted_slits.msh'/g' full_simulation.xml > 'XML_files/'$job_full_new'.xml'

  $path_openCFS 'XML_files/'$job_inc
  $path_openCFS 'XML_files/'$job_full
  $path_openCFS 'XML_files/'$job_full_new
done
