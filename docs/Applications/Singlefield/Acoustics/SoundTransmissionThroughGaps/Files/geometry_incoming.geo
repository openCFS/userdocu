
// Definition of constants

height=10;
width=5;
pmlW=1;
elemSize = 0.05;  
pos_middle=2.5;


// Bottom up modeling approach for structured mesh: Points → lines → surfaces → volumes → mesh

// Define basepoint for plate

Point(1) = {-width-pmlW, 0 , 0, 1.0};
Point(2) = {-width, 0 , 0, 1.0};
Point(3)={-pos_middle, 0 , 0, 1.0};
Point(4) = {0, 0 , 0, 1.0};
Point(5) = {pmlW, 0 , 0, 1.0};


Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 5};


Extrude{ 0, height, 0 }{ Line{1,2,3,4}; }

// Get rid of multiple entities and current states
Coherence;

// Generate regions to address later in CFS++
Physical Surface("s_air") = {12,16};
Physical Surface("s_pml_front") = {8};
Physical Surface("s_pml_back") = {20}; 

Physical Curve("c_top")={5,9,13,17};
Physical Curve("c_bottom")={1,2,3,4};
Physical Curve("c_rhs")={7};
Physical Curve("c_rightside")={15};
Physical Curve("c_middle")={11};


// Structured meshing and recombine triangles to quadrilaterals

Transfinite Surface '*';
Recombine Surface '*';

Transfinite Line {6,7,11,15,19} = ((height)/elemSize) Using Progression 1;
Transfinite Line {1,4,5,17} = ((pmlW)/elemSize) Using Progression 1;
Transfinite Line {2,9} = ((width-pos_middle)/elemSize) Using Progression 1;
Transfinite Line {3,13} = ((pos_middle)/elemSize) Using Progression 1;


// Perform 2D meshing with 1st order elements
Mesh.ElementOrder = 1;
Mesh 2; 